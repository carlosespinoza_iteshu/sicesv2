﻿using System;
using OpenNETCF.MQTT;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

namespace Server
{
    class Program
    {
        static MQTTClient mqtt;
        static Dictionary<string, string> personas;
        static void Main(string[] args)
        {
            CrearPersonas();
            mqtt =new MQTTClient("broker.hivemq.com",1883);
            mqtt.Disconnected += Mqtt_Disconnected;
            mqtt.MessageReceived += Mqtt_MessageReceived;
            mqtt.Subscriptions.Add(new Subscription("SiCES/#"));
            ConectarMQTT();
            mqtt.Publish("SiCES/Servers","ServerCarlos Conectado", QoS.FireAndForget,false);
            Console.ReadLine();
        }

        private static void Mqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            string mensaje = System.Text.Encoding.UTF8.GetString(payload);
            Console.WriteLine($"[{topic}:{DateTime.Now.ToShortTimeString()}] {mensaje}");
            if (topic == "SiCES/Registro")
            {
                string[] datos = mensaje.Split(' ');
                try
                {
                    string nombre = personas.Where(p => p.Key == datos[2]).SingleOrDefault().Value;
                    mqtt.Publish($"SiCES/{datos[3]}", nombre, QoS.FireAndForget, false);
                }
                catch
                {
                    mqtt.Publish($"SiCES/{datos[3]}", "No encontrado", QoS.FireAndForget, false);
                }
            }
        }

        private static void Mqtt_Disconnected(object sender, EventArgs e)
        {
            ConectarMQTT();
        }

        private static void CrearPersonas()
        {
            personas= new Dictionary<string, string>();
            personas.Add("d4f580fb", "Williams Gomez");
            personas.Add("84bc91fc", "Jose Reyes");
            personas.Add("a38e1689", "Aranza Lopez");
            personas.Add("abfc5673", "Carlos Espinoza");
        }

        private static void ConectarMQTT()
        {
            mqtt.Connect("ServerCarlos");
            Console.Write("Conectado a MQTT");
            while (!mqtt.IsConnected)
            {
                Thread.Sleep(500);
                Console.Write(".");
            }
            Console.WriteLine("\nConectado :)");
            Console.WriteLine("Esperando Mensajes, precione enter para terminar...");
        }       
    }

}
