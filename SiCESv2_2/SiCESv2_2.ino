#include<SPI.h>
#include<MFRC522.h>
#include<DS3232RTC.h>
#include <RTClib.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Streaming.h>
#include <WiFiManager.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <SD.h>

File myFile;

#define SS_PIN D4
#define RST_PIN D0
/*
const char* ssid = "Zero's WiFi"; // Enter your WiFi name
const char* password =  "qwertyuiop99"; // Enter WiFi password
*/
const char* mqttServer = "broker.hivemq.com";
const int mqttPort = 1883;
String sicesId="SiCES01";
String sicesLugar="01";
int esEntrada=1;
//const char* mqttUser = "otfxknod";
//const char* mqttPassword = "nSuUc1dDLygF";
String mensaje; 
WiFiClient espClient;
PubSubClient client(espClient);
WiFiManager wifiManager;
RTC_DS3231 rtc;
MFRC522 lectorRFID(SS_PIN,RST_PIN);
LiquidCrystal_I2C lcd(0x3F,16,2);
String texto; //variable a mostrar en LCD y serial
byte abajo[8]={
  B01110,
  B01110,
  B01110,
  B01110,
  B01110,
  B11111,
  B01110,
  B00100
};

byte arriba[8]=
{
  B00100,
  B01110,
  B11111,
  B01110,
  B01110,
  B01110,
  B01110,
  B01110

};

void PublicarMQTT(String topic,String mensaje){
  int len_topic=topic.length()+1;
  int len_mensaje=mensaje.length()+1;
  char t[len_topic];
  char m[len_mensaje];
  topic.toCharArray(t,len_topic);
  mensaje.toCharArray(m,len_mensaje);
  client.publish(t, m);
}
void callback(char* topic, byte* payload, unsigned int length) {

}
void ConectarInternetMqtt(){
  LimpiarPantalla();
  MostrarEnPantalla("Config WiFi",0,0);
  MostrarEnPantalla("-> 192.168.4.1",1,0);
  if(!wifiManager.autoConnect("SiCES01")){
    Serial.print("Fallo en la conexion (timeout)");
    ESP.reset();
    delay(1000);
  }
  LimpiarPantalla();
 MostrarEnPantalla("Conectando :)",1,0);
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
  LimpiarPantalla();
 MostrarEnPantalla("Conectando a MQTT...",0,0);
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP8266Carlos")) {
 
      Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
}
MostrarEnPantalla("Conectando a MQTT :)",0,0);
}

void setup() {
  Serial.begin(9600);
  pinMode(D3,OUTPUT);
  
  SPI.begin();
  lectorRFID.PCD_Init();
  

  Wire.begin();//Se inicializa comunicacion i2c
  rtc.begin();//Se inicializa el RTC
  delay(100);
  rtc.adjust(DateTime(__DATE__, __TIME__));

  lcd.init();
  lcd.backlight();
  ConectarInternetMqtt();
  LimpiarPantalla();
  Serial.println("RFID Listo");
  MostrarEnPantalla("Listo!!",0,0);
  MostrarEnPantalla("Bienvenido",1,esEntrada);
  PublicarMQTT("SiCES", sicesId+ String(" Conectado"));
}

String ObtenerFechaHora(){
  String dataString=""; //variable para la fecha Hora
  DateTime now = rtc.now();
  dataString += now.day();
  dataString += "/";
  dataString += now.month();
  dataString += "/";
  dataString += now.year();
  dataString += " ";
  dataString += now.hour();
  dataString += ":";
  dataString += now.minute();
  dataString += ":";
  dataString += now.second();
  return dataString;
}
String ObtenerTemperatura(){
   //return String(rtc.GetTemperature());
}

String ObtenerIdRFID(){
  String id="";
  //Serial.print(".");
  if(lectorRFID.PICC_IsNewCardPresent()){
    //Serial.print("Hay tarjeta"); 
  if(lectorRFID.PICC_ReadCardSerial()){
      for(byte i=0;i<lectorRFID.uid.size;i++){
        id+=lectorRFID.uid.uidByte[i]<0x10?"0": "";
        id+=String(lectorRFID.uid.uidByte[i],HEX);
      }
      lectorRFID.PICC_HaltA();
    }
  }
  return id;
}

void MostrarEnPantalla(String texto,int linea,int esEntrada){
  lcd.setCursor(0,linea);
  lcd.print(texto);
  
  if(esEntrada==1){
    lcd.createChar(0,arriba);
    lcd.setCursor(15,linea);
    lcd.write((byte)0);
  }
  if(esEntrada==2){
    lcd.createChar(0,abajo);
    lcd.setCursor(15,linea);
    lcd.write((byte)0);
  }
}
void LimpiarPantalla(){
  lcd.clear();
}

void SonarBuzzer(){
  digitalWrite(D3,HIGH);
  delay(200);
  digitalWrite(D3,LOW);
}

void EscribirArchivo(String nombreArchivo,String contenido){
  Serial.print("Initializing SD card...");

  if (!SD.begin(D8)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open(nombreArchivo, FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to "+nombreArchivo+"...");
    myFile.println(contenido);
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening "+nombreArchivo);
  }

  // re-open the file for reading:
  myFile = SD.open(nombreArchivo);
  if (myFile) {
    Serial.println(nombreArchivo+":");

    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening "+nombreArchivo);
  }
}

void loop() {
  client.loop();
  String id=ObtenerIdRFID();
  if(id!=""){
    
    texto=ObtenerFechaHora();
    texto+=" ";
    texto+=id;
    texto+=" ";
    texto+=sicesId;
    texto+=" ";
    texto+=sicesLugar;
    texto+=" ";
    texto+=String(esEntrada);
    //texto+= " " + ObternerTemperatura();
    Serial.println(texto);
    LimpiarPantalla();
    MostrarEnPantalla(ObtenerFechaHora(),0,esEntrada);
    MostrarEnPantalla(id,1,0);
    SonarBuzzer();
    //EscribirArchivo("SiCES.txt",texto);
    PublicarMQTT("SiCES",texto);
  }
}
