﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using SiCESv2.COMMON.Entidades;

namespace SiCESv2.COMMON.Validadores
{
    public class GenericValidator<T>:AbstractValidator<T> where T:BaseDTO
    {
        public GenericValidator()
        {
            RuleFor(entidad => entidad.Id).NotNull().NotEmpty();
            RuleFor(entidad=>entidad.FechaHora).NotEmpty().NotNull();
        }
    }
}