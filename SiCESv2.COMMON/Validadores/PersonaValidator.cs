﻿using SiCESv2.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace SiCESv2.COMMON.Validadores
{
    public class PersonaValidator:GenericValidator<Persona>
    {
        public PersonaValidator()
        {
            RuleFor(p => p.ApMaterno).NotEmpty().NotNull();
            RuleFor(p => p.ApPaterno).NotEmpty().NotNull();
            RuleFor(p => p.Email).EmailAddress().NotNull().NotEmpty();
            RuleFor(p => p.IdCarrera).NotEmpty().NotNull();
            RuleFor(p => p.Matricula).NotEmpty().NotNull();
            RuleFor(p=>p.Nombre).NotEmpty().NotNull();
            RuleFor(p => p.Sexo).NotNull().NotEmpty().Equal('H').Equal('M');
            RuleFor(p=>p.Telefono).NotEmpty().NotNull();
        }
    }
}
