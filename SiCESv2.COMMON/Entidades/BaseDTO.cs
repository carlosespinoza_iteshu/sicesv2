﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiCESv2.COMMON.Entidades
{
    public class BaseDTO
    {
        public string Id { get; set; }
        public DateTime FechaHora { get; set; }

    }
}
