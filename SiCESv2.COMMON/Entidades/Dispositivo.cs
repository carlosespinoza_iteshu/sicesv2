﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiCESv2.COMMON.Entidades
{
    public class Dispositivo:BaseDTO
    {
        public string Nombre { get; set; }
        public string IdLugar { get; set; }
        public string Notas { get; set; }
    }
}
